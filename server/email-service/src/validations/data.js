'use strict';

const Joi = require('joi');

const phoneRegex = /(\d{3}\s\d{3}\s\d{4})/;
const nameRegex = /(([A-Za-z]*[\-\s]?)[A-Za-z]*)*/;

const user = Joi.object({
    _id: Joi.string().alphanum(),
    phone: Joi.string().regex(phoneRegex).length(12),
    email: Joi.string().email().required(),
    first: Joi.string().regex(nameRegex).required(),
    last: Joi.string().regex(nameRegex),
});

const record = Joi.object({
    points: Joi.number().required(),
    checkIns: Joi.number(),
    lastCheckIn: Joi.number().required(),
});

const schema = Joi.object({ user, record });

module.exports = (data) => Joi.validate(data, schema);
