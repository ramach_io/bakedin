'use strict';

const status = require('http-status');

const validate = require('./validations');
const sendEmail = require('./mailer');

const error400 = status.BAD_REQUEST;
const error422 = status.UNPROCESSABLE_ENTITY;
const error500 = status.INTERNAL_SERVER_ERROR;

module.exports = (app) => {
    app.post('/', (req, res) => (
        validate(req.body)
        .then(data => sendEmail(data)
            .then(message => res.status(status.OK).json(message))
            .catch(err => res.status(error500).json(err))
        ).catch(err => res.status(error422).json(err))
    ));

    app.all('*', (req, res) => res.sendStatus(error400));
};
