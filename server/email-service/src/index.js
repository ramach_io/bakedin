'use strict';

const routes = require('./routes');
const server = require('./server');

const port = process.env.EMAIL_PORT || 5000;

server.start({ port, routes });
