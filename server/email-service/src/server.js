'use strict';

const express = require('express');
const bodyParser = require('body-parser');

module.exports = {
    start: ({ port, routes }) => {
        const app = express();
        app.use(bodyParser.json());
        routes(app);
        app.listen(port);
        console.log(`Email service started on port ${port}`);
    },
};
