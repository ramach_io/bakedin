'use strict';

const mailer = require('nodemailer');

const successMessage = 'Email sent to ';
const waitMessage = 'Inside 5 minutes. Can\'t check in again yet.';

module.exports = ({ user, record }) => (
    new Promise((resolve, reject) => {
        if (record.lastCheckIn + 300000 > Date.now()) {
            console.log(waitMessage);
            return resolve(waitMessage);
        };

        /** An email sent by following code may not be delivered.
         *  Please see below for details. */

        mailer.createTestAccount((err, account) => {
            if (err) {
                return reject(err);
            };

            const transport = mailer.createTransport({
                host: account.smtp.host,
                port: account.smtp.port,
                secure: account.smtp.secure,
                auth: {
                    user: account.user,
                    pass: account.pass,
                },
                logger: false,
                debug: false,
            }, {
                from: 'BakedIn Loyalty Rewards <no-reply@bakedin.fake>',
            });

            const message = {
                to: user.email,
                subject: `Thanks for checking in, ${user.first}!!`,
                html: `
                    <h1>
                        You've earned <b>${record.points} POINTS</b> so far!!
                    </h1>
                `,
            };

            const sendSuccessMessage = (info) => (
                `${successMessage}${user.first} ${user.last} <${user.email}>\n${info}`
            );

            return transport.sendMail(
                message,
                (err, info) => {
                    if (err) {
                        console.log('FAILED TO SEND', err);
                        return reject(err);
                    };

                    const logMessage = sendSuccessMessage(JSON.stringify(info));
                    console.log(logMessage);
                    return resolve(logMessage);
                }
            );
        });

        /** Security policies of most major email providers will prevent delivery
         *  from randomly generated nodemailer addresses like the one above.
         *  The code below, if uncommented, can demonstrate a real delivery scenario.
         *  To see the delivered email visit https://ethereal.email/ and log in with:
         * 
         *      EMAIL: dxnywamjcrhtrtvk@ethereal.email
         *      PASSWORD: czeQe2qks2rbM8qdtn
         * 
         *  Uncomment the code below, comment out above, then rebuild and run the app. */

        // const transport = mailer.createTransport({
        //     host: 'ethereal.email',
        //     port: 587,
        //     secure: false,
        //     auth: {
        //         user: 'dxnywamjcrhtrtvk',
        //         pass: 'czeQe2qks2rbM8qdtn',
        //     },
        //     logger: false,
        //     debug: false,
        // }, {
        //     from: 'BakedIn Loyalty Rewards <dxnywamjcrhtrtvk@ethereal.email>',
        // });

        // const message = {
        //     to: 'dxnywamjcrhtrtvk@ethereal.email',
        //     subject: `Thanks for checking in, ${user.first}!!`,
        //     html: `
        //         <h1>
        //             You've earned <b>${record.points} POINTS</b> so far!!
        //         </h1>
        //     `,
        // };

        // const sendSuccessMessage = (info) => (
        //     `${successMessage}${user.first} ${user.last} <${user.email}>\n${info}`
        // );

        // return transport.sendMail(
        //     message,
        //     (err, info) => {
        //         if (err) {
        //             console.log('FAILED TO SEND', err);
        //             return reject(err);
        //         };

        //         const logMessage = sendSuccessMessage(JSON.stringify(info));
        //         console.log(logMessage);
        //         return resolve(logMessage);
        //     }
        // );
    })
);
