'use strict';

const status = require('http-status');

const validate = require('./validations');
const { getOrCreateUser } = require('./logic');

const error400 = status.BAD_REQUEST;
const error422 = status.UNPROCESSABLE_ENTITY;
const error500 = status.INTERNAL_SERVER_ERROR;

module.exports = ({ app, repo }) => {
    app.post('/', (req, res) => (
        validate(req.body)
        .then(data => (
            getOrCreateUser(data)(
                repo.getUserByPhone,
                repo.createUser
            ).then(user => res.status(status.OK).json(user))
            .catch(err => (
                res.status(error500).json({
                    status: error500,
                    message: 'ERROR: USER->DB',
                    error: err,
                })
            ))
        )).catch(err => res.status(error422).json({
            status: error422,
            message: 'ERROR: USER<->VALIDATE',
            error: err
        }))
    ));

    app.all('*', (req, res) => res.sendStatus(error400));
};
