'use strict';

const {
  getOrCreateUser,
} = require('./logic');

const create = (user) => Promise.resolve(user);
const first = 'Leonard';
const last = 'Cohen';
const email = 'len@hallelu.jah';
const phone = '234 555 6789';

const userAccount = {
  _id: 101,
  first,
  last,
  email,
  phone,
};

describe(`UNIT ${__dirname}`, () => {
  describe('getOrCreateUser()', () => {
    describe('User has missing fields, user found', () => {
      const get = phone => Promise.resolve(userAccount);

      test('should return user', () => {
        const user = {
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });

      test('should return user', () => {
        const user = {
          email,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });

      test('should return user', () => {
        const user = {
          last,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });

      test('should return user', () => {
        const user = {
          last,
          email,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });

      test('should return user', () => {
        const user = {
          first,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });

      test('should return user', () => {
        const user = {
          first,
          email,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });

      test('should return user', () => {
        const user = {
          first,
          last,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });
    });

    describe('User has missing fields, user not found', () => {
      const get = user => Promise.resolve(null);

      test('should return user phone', () => {
        const user = {
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual({ phone })
        );
      });

      test('should return user phone', () => {
        const user = {
          email,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual({ phone })
        );
      });

      test('should return user phone', () => {
        const user = {
          last,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual({ phone })
        );
      });

      test('should return user phone', () => {
        const user = {
          last,
          email,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual({ phone })
        );
      });

      test('should return user phone', () => {
        const user = {
          first,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual({ phone })
        );
      });

      test('should return user phone', () => {
        const user = {
          first,
          email,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual({ phone })
        );
      });

      test('should return user phone', () => {
        const user = {
          first,
          last,
          phone,
        };

        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual({ phone })
        );
      });
    });

    describe('User has all fields', () => {
      const get = phone => Promise.resolve(userAccount);

      const user = {
        first,
        last,
        email,
        phone,
      };

      test('should create a user', () => {
        getOrCreateUser(user)(get, create).then(
          res => expect(res).toEqual(userAccount)
        );
      });
    });
  });
});
