'use strict';

const getOrCreateUser = (user) => (get, create) => (
    !user.first || !user.last || !user.email ? (
        get(user.phone)
        .then(account => (
            account ? account : { phone: user.phone }
        ))
    ) : (
        create(user)
        .then(() => get(user.phone))
    )
);

module.exports = {
    getOrCreateUser,
};
