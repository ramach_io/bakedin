'use strict';

const Joi = require('joi');

const phoneRegex = /(\d{3}\s\d{3}\s\d{4})/;
const nameRegex = /(([A-Za-z]*[\-\s]?)[A-Za-z]*)*/;

const schema = Joi.object({
    phone: Joi.string().regex(phoneRegex).length(12).required(),
    email: Joi.string().email(),
    first: Joi.string().regex(nameRegex),
    last: Joi.string().regex(nameRegex),
});

module.exports = (data) => Joi.validate(data, schema);
