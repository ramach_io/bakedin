'use strict';

const routes = require('./routes');
const server = require('./server');
const database = require('./database');
const repository = require('./repository');

const port = process.env.USER_PORT || 9000;

server.start({ port, routes, database, repository });
