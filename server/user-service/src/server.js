'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const collections = ['user'];

module.exports = {
    start: ({ port, routes, database, repository }) => {
        database.connect()
        .then(db => database.init(db, collections))
        .then(db => {
            const app = express();
            app.use(bodyParser.json());
            const repo = repository(db);
            routes({ app, repo });
            app.listen(port);
            console.log(`User service started on port ${port}`);
        }).catch(err => console.log(err));
    },
};
