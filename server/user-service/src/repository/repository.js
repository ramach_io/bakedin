'use strict';

module.exports = (db) => {
    const collection = db.collection('user');

    const getUserByPhone = (phone) => collection.findOne({ phone });

    const createUser = (user) => collection.updateOne(
        { phone: user.phone },
        { $set: user },
        { upsert: true }
    );

    return {
        getUserByPhone,
        createUser,
    };
};
