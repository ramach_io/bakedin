'use strict';

const routes = require('./routes');
const server = require('./server');

const domains = {
    user: process.env.USER_DOMAIN || 'http://user-service',
    checkIn: process.env.CHECKIN_DOMAIN || 'http://checkin-service',
    email: process.env.EMAIL_DOMAIN || 'http://email-service',
    gateway: process.env.GATEWAY_DOMAIN || 'http://gateway-service',
};

const ports = {
    user: process.env.USER_PORT || 9000,
    checkIn: process.env.CHECKIN_PORT || 7000,
    email: process.env.EMAIL_PORT || 5000,
    gateway: process.env.GATEWAY_PORT || 3000,
};

server.start({ domains, ports, routes });
