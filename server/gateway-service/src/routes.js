'use strict';

const status = require('http-status');

const { request, error } = require('./utils');
const validate = require('./validations');

const error400 = status.BAD_REQUEST;
const error422 = status.UNPROCESSABLE_ENTITY;
const error500 = status.INTERNAL_SERVER_ERROR;

module.exports = ({ domains, ports, app }) => {
    const reqUser = request(domains.user, ports.user);
    const reqCheckIn = request(domains.checkIn, ports.checkIn);
    const reqEmail = request(domains.email, ports.email);

    app.post('/', (req, res) => (
        validate(req.body)
        .then(data => (
            reqUser.post('/', data)
            .then(({ data: user }) => (
                user._id ? (
                    reqCheckIn.post(`/`, { _id: user._id })
                    .then(({ data: record }) => {
                        reqEmail.post('/', { user, record })
                        .catch(err => (
                            console.warn(JSON.stringify(
                                error(err, 'WARN: Problem sending email')
                            ))
                        ));
                        return res.status(status.OK).json({ record });
                    }).catch(err => (
                        res.status(error500).json(
                            error(err, 'ERROR: GATEWAY->CHECKIN')
                        )
                    ))
                ) : res.status(status.OK).json(user)
            )).catch(err => (
                res.status(error500).json(
                    error(err, 'ERROR: GATEWAY->USER')
                )
            ))
        ))
        .catch(err => (
            res.status(error422).json({
                status: error422,
                message: 'ERROR: GATEWAY<->VALIDATE',
                error: err,
            })
        ))
    ));

    app.all('*', (req, res) => {
        res.sendStatus(error400);
    });
};
