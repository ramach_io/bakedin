'use strict';

module.exports = ({
    response: {
        status,
        statusText,
        headers: {
            date,
        },
        config: {
            timeout,
            method,
            url,
            data,
        },
    },
}, message) => ({
    status,
    statusText,
    date,
    timeout,
    method,
    url,
    data,
    message,
});
