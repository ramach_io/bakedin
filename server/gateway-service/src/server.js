'use strict';

const express = require('express');
const bodyParser = require('body-parser');

module.exports = {
    start: ({ domains, ports, routes }) => {
        const app = express();
        app.use(bodyParser.json());
        routes({ domains, ports, app });
        app.listen(ports.gateway);
        console.log(`Gateway started on port ${ports.gateway}`);
    },
};
