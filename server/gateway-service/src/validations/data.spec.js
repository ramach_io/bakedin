'use strict';

const validate = require('./data');

const validData = {
  phone: '234 555 6789',
  first: 'Leonard',
  last: 'Cohen',
  email: 'len@hallelu.jah',
};

describe(`UNIT ${__dirname}`, () => (
  describe('validate()', () => (
    describe('Given valid user data', () => (
      test('should validate', () => (
        validate(validData).then(
          res => expect(res).toEqual(validData)
        )
      ))
    ))
  ))
));
