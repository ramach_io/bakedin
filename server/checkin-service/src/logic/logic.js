'use strict';

const { ObjectID } = require('mongodb');

const addUpPoints = (matches) => matches.reduce((acc, match) => (
    match.points + acc
), 0);

const getLastCheckIn = (matches) => matches.reduce((acc, match) => (
    match.time > acc ? match.time : acc
), 0);

const canCheckIn = (last) => last + 300000 < Date.now();

const getPoints = (checkIns, points = 0) => (
    checkIns ? points + 20 : points + 50
);

const aggregateRecord = ({ _id }) => (get, insert) => (
    get({ userId: _id, vendorId: 1 })
    .then(matches => ({
        points: addUpPoints(matches),
        checkIns: matches.length,
        lastCheckIn: getLastCheckIn(matches),
    })).then(record => (
        canCheckIn(record.lastCheckIn) ? (
            insert({
                userId: _id,
                vendorId: 1,
                points: getPoints(record.checkIns),
                time: Date.now(),
            })
            .then(() => ({
                points: getPoints(record.checkIns, record.points),
                checkIns: record.checkIns + 1,
                lastCheckIn: record.lastCheckIn,
            }))
        ) : record
    ))
);

module.exports = {
    aggregateRecord,
};
