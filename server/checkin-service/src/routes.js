'use strict';

const status = require('http-status');

const validate = require('./validations');
const { aggregateRecord } = require('./logic');
const {
    getRecord,
    addCheckIn,
} = require('./repository');

const error400 = status.BAD_REQUEST;
const error422 = status.UNPROCESSABLE_ENTITY;
const error500 = status.INTERNAL_SERVER_ERROR;

module.exports = ({ app, repo }) => {
    app.post('/', (req, res) => validate(req.body)
    .then(data => (
        aggregateRecord(data)(
            repo.getRecord,
            repo.addCheckIn
        ).then(record => (
            res.status(status.OK).json(record)
        )).catch(err => res.status(error500).json(err))
    )).catch(err => res.status(error422).json(err)));

    app.all('*', (req, res) => res.sendStatus(error400));
};
