'use strict';

module.exports = (db) => {
    const collection = db.collection('checkIn');

    const getRecord = (data) => collection.find(data).toArray();

    const addCheckIn = (data) => collection.insertOne(data);

    return {
        getRecord,
        addCheckIn,
    };
};
