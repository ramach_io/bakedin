'use strict';

const routes = require('./routes');
const server = require('./server');
const database = require('./database');
const repository = require('./repository');

const port = process.env.CHECKIN_PORT || 7000;

server.start({ port, routes, database, repository });
