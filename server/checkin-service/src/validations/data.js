'use strict';

const Joi = require('joi');

const schema = Joi.object({
    _id: Joi.string().alphanum(),
});

module.exports = (data) => Joi.validate(data, schema);
