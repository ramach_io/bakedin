'use strict';

const mongo = require('mongodb');

const url = process.env.MONGO_URL || 'mongodb://mongo:27017';

module.exports = {
    connect: () => new Promise((resolve, reject) => {
        const MongoClient = mongo.MongoClient;

        MongoClient.connect(url, (err, db) => {
            if (err) {
                console.log('MONGO CONNECT ERR', err);
                return reject(err);
            };

            return resolve(db.db('bakedin'));
        });
    }),
    init: (db, collections) => (
        Promise.all(collections.map(name => (
            db.collection(name).insertOne({
                _init_: 'init',
            }).then(res => console.log(
                    `<DB>${name} INSERT: ${res}`
            )).then(() => (
                db.collection(name).deleteMany({
                    _init_: 'init'
                }).then(res => console.log(
                    `<DB>${name} DELETE: ${res}`
                ))
            ))
        ))).then(res => console.log(
            `<DB>INIT SUCCESSFUL ${res}`
        )).then(() => db)
        .catch(err => console.log(
            `<DB>INIT FAILED: ${err}`
        ))
    ),
};
