'use strict';

const express = require('express');
const status = require('http-status');
const path = require('path');
const bodyParser = require('body-parser');

const { request } = require('./utils');

const kioskPort = process.env.KIOSK_PORT || 8080;
const gatewayPort = process.env.GATEWAY_PORT || 3000;

const reqApi = request('http://gateway-service', gatewayPort);

const app = express();

app.use(bodyParser.json());

app.use(express.static(__dirname + '/dist'));

app.get('/', (req, res) => (
	res.sendFile(path.resolve(__dirname, 'dist', 'index.html'))
));

app.post('/', (req, res) => (
	reqApi.post('/', req.body)
	.then(response => {
		return response;
	}).then(response => res.status(status.OK).json(response.data))
	.catch(err => (
		res.status(err.response.data.status).json(
			err.response.data
		)
	))
));

app.listen(kioskPort);
console.log(`Kiosk started on kioskPort ${kioskPort}`);
