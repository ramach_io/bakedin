const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: [
        // 'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        './src/index.js',
    ],
    module: {
        rules:[{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: [
                'react-hot-loader/webpack',
                'babel-loader?presets[]=react'
            ],
        },
        {
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader',
            ],
        }],
    },
    resolve: {
        extensions: ['.jsx', '.js'],
        modules: [
            'src',
            'node_modules'
        ],
    },
    output: {
        path: `${__dirname}/dist`,
        publicPath: '/',
        filename: 'bundle.js',
    },
    devServer: {
        contentBase: './dist',
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            'React': 'react',
        }),
    ],
};
