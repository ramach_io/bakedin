'use strict';

const axios = require('axios');

const make = (domain, url, port) => (
    `${domain}${port && ':' + port}${url}`
);

module.exports = (domain, port) => ({
    get: (url) => axios.get(make(domain, url, port)),
    post: (url, data) => axios.post(make(domain, url, port), data),
    put: (url, data) => axios.put(make(domain, url, port), data),
    patch: (url, data) => axios.patch(make(domain, url, port), data),
    delete: (url, data) => axios.delete(make(domain, url, port), data),
});
