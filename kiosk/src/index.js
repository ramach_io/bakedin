'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const { createStore, applyMiddleware } = require('redux');
const { Provider } = require('react-redux');
const { default: thunk } = require('redux-thunk');

const reducer = require('./reducers');
const { App } = require('./presentation/components/App');

const configureStore = () => {
  const store = createStore(reducer, applyMiddleware(thunk));

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextReducer = require('./reducers');
      store.replaceReducer(nextReducer);
    })
  };

  return store;
};

const store = configureStore();

if (typeof window !== 'undefined') {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('main')
  );
};
