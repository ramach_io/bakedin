'use strict';

const { getViewInit, timeLeftInit } = require('./logic');
const { Phone } = require('../components/Phone');
const { Registration } = require('../components/Registration');
const { Record } = require('../components/Record');

const getView = getViewInit({ Phone, Registration, Record });

const interval = 120000;
const minutesLabel = 'minutes';
const secondsLabel = 'seconds';

const timeLeft = timeLeftInit(interval, minutesLabel, secondsLabel);

module.exports = {
  getView,
  timeLeft,
};
