'use strict';

const {
  getViewInit,
  timeLeftInit,
} = require('./logic');

describe(`UNIT ${__dirname}`, () => {
  const views = {
    Phone: 'Phone',
    Registration: 'Registration',
    Record: 'Record',
  };

  const getView = getViewInit(views);

  describe('getView()', () => {
    describe('No record or no phone number in props', () => {
      const props = {};
      test('should return Phone component view', () => (
        expect(getView(props)).toEqual('Phone')
      ));
    });

    describe('Phone number but no record in props', () => {
      const props = { phone: '555 555 5555' };
      test('should return Registration component view', () => (
        expect(getView(props)).toEqual('Registration')
      ));
    });

    describe('Record in props', () => {
      const props = { record: 'User record...' };
      test('should return Record component view', () => (
        expect(getView(props)).toEqual('Record')
      ));
    });
  });

  describe('timeLeft()', () => {
    const interval = 300000;
    const minutes = 'mins';
    const seconds = 'secs';
    const now = 1525145890;

    const timeLeft = timeLeftInit(interval, minutes, seconds);

    describe('More than 5 minutes', () => {
      const last = now - (60000 * 7);
      test('should return null', () => (
        expect(!!timeLeft(now, last)).toEqual(false)
      ));
    });

    describe('Less than 5 minutes, more than 1 minute', () => {
      const last = now - (300000 - (60000 * 3.5));
      test('should return a value in minutes', () => (
        expect(timeLeft(now, last)).toEqual('3 mins')
      ));
    });

    describe('Less than 1 minute', () => {
      const last = now - (300000 - (1000 * 37));
      test('should return a value in seconds', () => (
        expect(timeLeft(now, last)).toEqual('37 secs')
      ));
    });
  });
});
