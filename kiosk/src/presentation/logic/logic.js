'use strict';

const getViewInit = (views) => (props) => (
  props.record ? views.Record : (
    props.phone ? views.Registration : views.Phone
  )
);

const timeLeftInit = (interval, minutes, seconds) => (now, last) => (
  last + interval > now ? (
    last + interval - now > 60000 ? `${
      Math.floor((last + interval - now) / 60000)
      } ${minutes}` : `${
      Math.floor((last + interval - now) % 60000 / 1000)
      } ${seconds}`
  ) : null
);

module.exports = {
  getViewInit,
  timeLeftInit,
};
