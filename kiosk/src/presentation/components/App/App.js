'use strict';

const React = require('react');
const { connect } = require('react-redux');

const { MuiThemeProvider } = require('material-ui/styles');

const { getView } = require('../../logic');

const App = (props) => {
  const View = getView(props);
  return (
    <div className='AppComponent'>
      <MuiThemeProvider>
        <View />
      </MuiThemeProvider>
    </div>
  )
};

const mapStateToProps = (state) => ({
  phone: state.user.phone,
  record: state.user.record,
});

module.exports = connect(mapStateToProps)(App);
