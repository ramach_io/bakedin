'use strict';

const React = require('react');
const { useState } = React;
const { connect } = require('react-redux');

const { default: Paper } = require('material-ui/Paper');
const { default: TextField } = require('material-ui/TextField');
const { default: RaisedButton } = require('material-ui/RaisedButton');
const { default: InputMask } = require('react-input-mask');

const { findUserByPhone } = require('../../../actions/user');

const {
  paperStyle,
  textStyle,
  textInputStyle,
  underlineInputStyle,
  errorInputStyle,
  buttonStyle,
} = require('./styles');

const phoneInputLabel = 'Enter your account phone number';
const phoneErrorMessage = 'Please enter a valid US phone number';
const submitButtonLabel = 'CHECK IN!';

const Phone = ({ dispatch }) => {
  const [phone, setPhone] = useState('');
  const [phoneError, setPhoneError] = useState();

  const handleTextChange = ({ target }) => setPhone(target.value)
  const handleSubmit = () => phone.length < 12 ? (
    setPhoneError(phoneErrorMessage)
  ) : dispatch(findUserByPhone(phone));

  return (
    <div className='PhoneComponent'>
      <Paper
        style={paperStyle}
      >
        <span
          style={textStyle}
        >
          {phoneInputLabel}
        </span>
        <TextField
          id='phone'
          value={phone}
          errorText={phoneError}
          inputStyle={textInputStyle}
          underlineFocusStyle={underlineInputStyle}
          errorStyle={errorInputStyle}
          onChange={handleTextChange}
        >
          <InputMask mask="999 999 9999" maskChar="" />
        </TextField>
        <br />
        <RaisedButton
          secondary
          label={submitButtonLabel}
          style={buttonStyle}
          onClick={handleSubmit}
        />
      </Paper>
    </div>
  );
};

const mapStateToProps = () => ({});

module.exports = connect(mapStateToProps)(Phone);
