'use strict';

module.exports = {
    paperStyle: {
        height: 170,
        width: 420,
        paddingTop: 30,
        textAlign: 'center',
        margin: '0 auto',
        marginTop: '10%'
    },

    textStyle: {
        fontWeight: 900,
        color: 'darkgrey',
    },

    titleStyle: {
        fontWeight: 'bold',
        color: 'steelblue',
        fontSize: 'x-large',
    },

    buttonStyle: {
        margin: 10,
    },
};
