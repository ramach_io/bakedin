'use strict';

const React = require('react');
const { connect } = require('react-redux');

const { default: Paper } = require('material-ui/Paper');
const { default: RaisedButton } = require('material-ui/RaisedButton');

const { startOver } = require('../../../actions/user');

const {
  paperStyle,
  textStyle,
  titleStyle,
  buttonStyle,
} = require('./styles');

const interval = 120000;

const timeLeft = (now, last) => (
  last + interval > now ? (
    last + interval - now > 60000 ? `${
      Math.ceil((last + interval - now) / 60000)
      } minutes` : `${
      Math.floor((last + interval - now) % 60000 / 1000)
      } seconds`
  ) : null
);

const successButtonLabel = 'GOT IT!';
const successMessage = 'Thanks for checking in!';
const nextTimeMessage = time => (
  `You can check in again in ${time}.`
);
const checkInNumberMessage = (checkIns) => (
  `CHECK-IN # ${checkIns}`
);
const checkInTotalMessage = (checkIns) => (
  `Total Check-Ins: ${checkIns}`
);

const Record = props => {
  const {
    record: {
      points = 0,
      checkIns = 0,
      lastCheckIn = 0,
    },
    dispatch,
  } = props;

  const countdown = timeLeft(Date.now(), lastCheckIn);

  const checkInMessage = countdown ? (
    nextTimeMessage(countdown)
  ) : successMessage;

  const countMessage = countdown ? (
    checkInTotalMessage(checkIns)
  ) : checkInNumberMessage(checkIns);

  const balanceMessage = (
    `You've earned ${points} points so far!`
  );

  const handleGoBack = () => dispatch(startOver());

  return (
    <div className='RecordComponent'>
      <Paper
        style={paperStyle}
      >
        <span
          style={titleStyle}
        >
          {countdown ? balanceMessage : countMessage}
        </span>
        <br />
        <span
          style={titleStyle}
        >
          {countdown ? countMessage : balanceMessage}
        </span>
        <br />
        <span
          style={textStyle}
        >
          {checkInMessage}
        </span>
        <br />
        <RaisedButton
          secondary
          label={successButtonLabel}
          style={buttonStyle}
          onClick={handleGoBack}
        />
      </Paper>
    </div>
  );
};

const mapStateToProps = (state) => ({
  record: state.user.record,
});

module.exports = connect(mapStateToProps)(Record);
