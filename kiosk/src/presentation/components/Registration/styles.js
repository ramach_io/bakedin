'use strict';

module.exports = {
    paperStyle: {
        height: 365,
        width: 420,
        paddingTop: 27,
        textAlign: 'center',
        margin: '0 auto',
        marginTop: '10%'
    },
    textStyle: {
        fontWeight: 900,
        color: 'darkgrey',
    },
    textInputStyle: {
        textAlign: 'center',
        color: 'steelblue',
        fontWeight: 'bold',
    },
    underlineInputStyle: {
        borderColor: 'steelblue',
    },
    errorInputStyle: {
        color: 'gold',
    },
    buttonStyle: {
        margin: 10,
    },
};
