'use strict';

const React = require('react');
const { useState } = React;
const { connect } = require('react-redux');

const { default: Paper } = require('material-ui/Paper');
const { default: RaisedButton } = require('material-ui/RaisedButton');

const RegistrationField = require('./RegistrationField');

const {
  registerUser,
  startOver,
} = require('../../../actions/user');

const {
  paperStyle,
  buttonStyle,
} = require('./styles');

const fields = {
  first: {
    label: 'First Name',
    required: true,
  },
  last: {
    label: 'Last Name',
    required: true,
  },
  email: {
    label: 'Email',
    required: true,
  },
};

const backButtonLabel = '< GO BACK';
const submitButtonLabel = 'REGISTER ';
const requiredMessage = 'This field is required.';

const makeFieldStates = (fields, props = {}) => (
  Object.keys(fields).map(key => {
    const { label, required } = fields[key];
    const state = useState(props[key] || '');
    const errorState = useState();

    return {
      id: key,
      label,
      required,
      get: () => state[0],
      set: state[1],
      getError: () => errorState[0],
      setError: errorState[1],
    };
  })
);

const Registration = props => {
  const { phone, dispatch } = props;

  const states = makeFieldStates(fields);

  const handleGoBack = () => dispatch(startOver());

  const handleSubmit = states => () => {
    const values = states.reduce((acc, state) => {
      const value = state.get();

      return state.required && !value ? (
        state.setError(requiredMessage) && false
      ) : acc && {
        ...acc,
        [state.id]: value,
      };
    }, { phone });

    if (values) {
      dispatch(registerUser(values))
    };
  };

  return (
    <div className='RegistrationComponent'>
      <Paper
        style={paperStyle}
      >
        {states.map(state => <RegistrationField {...state} key={state.id} />)}
        <RaisedButton
          primary
          label={backButtonLabel}
          style={buttonStyle}
          onClick={handleGoBack}
        />
        <RaisedButton
          secondary
          label={submitButtonLabel}
          style={buttonStyle}
          onClick={handleSubmit(states)}
        />
      </Paper>
    </div>
  );
};

const mapStateToProps = (state) => ({
  phone: state.user.phone,
});

module.exports = connect(mapStateToProps)(Registration);
