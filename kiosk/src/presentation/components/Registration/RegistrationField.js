'use strict';

const React = require('react');

const { default: TextField } = require('material-ui/TextField');

const {
  textStyle,
  textInputStyle,
  underlineInputStyle,
  errorInputStyle,
} = require('./styles');

const handleTextChange = set => ({ target }) => set(target.value);

module.exports = props => {
  const {
    id,
    label,
    get,
    set,
    getError,
  } = props;

  return (
    <div>
      <span
        style={textStyle}
      >
        {`${label}:`}
      </span>
      <br />
      <TextField
        id={id}
        value={get()}
        errorText={!get() && getError()}
        inputStyle={textInputStyle}
        underlineFocusStyle={underlineInputStyle}
        errorStyle={errorInputStyle}
        onChange={handleTextChange(set)}
      />
      <br />
    </div>
  );
};