'use strict';

const { request } = require('../../utils');
const axios = require('axios');

const reqApi = request('http://localhost', 8080);

const findUserByPhone = (phone) => (
    (dispatch) => reqApi.post('/', { phone })
    .then(res => (
        dispatch({
            type: 'USER_DATA',
            payload: res.data,
        })
    ))
    .catch(err => {
        console.log('ERR ON FIND USER BY PHONE', err);
    })
);

const startOver = () => (
    (dispatch) => dispatch({
        type: 'USER_DATA',
        payload: {},
    })
);

const registerUser = (data) => (
    (dispatch) => reqApi.post('/', data)
    .then(res => (
        dispatch({
            type: 'USER_DATA',
            payload: res.data,
        })
    ))
    .catch(err => {
        console.log('ERR ON REGISTER USER', err);
    })
);

module.exports = {
    findUserByPhone,
    startOver,
    registerUser,
};
