'use strict';

const userReducer = require('./user');

describe(`UNIT ${__dirname}`, () => {
  const previousUserState = {
    phone: '777 555 3333',
    record: {
      points: 90,
      checkIns: 3,
      lastCheckIn: 1525145890,
    },
  };

  const expectedUserState = {
    phone: '333 555 7777',
    record: {
      points: 110,
      checkIns: 4,
      lastCheckIn: 1525845890,
    },
  };

  const defaultUserState = {};

  describe('userReducer()', () => {
    describe('Given USER_DATA action', () => {
      const userDataAction = {
        type: 'USER_DATA',
        payload: {
          phone: expectedUserState.phone,
          record: expectedUserState.record,
        },
      };

      test('should return an updated state', () => (
        expect(userReducer(previousUserState, userDataAction))
          .toEqual(expectedUserState)
      ));
    });

    describe('Given a different action', () => {
      const otherAction = {
        type: 'OTHER_ACTION',
        payload: {
          foo: 'bar',
        },
      };

      test('should return the previous state', () => (
        expect(userReducer(previousUserState, otherAction))
          .toEqual(previousUserState)
      ));
    });

    describe('Given no previous state', () => {
      const otherAction = {
        type: 'ANOTHER_ACTION',
        payload: {
          boo: 'far',
        },
      };

      test('should return the default state', () => (
        expect(userReducer(undefined, otherAction))
          .toEqual(defaultUserState)
      ));
    });
  });
});
