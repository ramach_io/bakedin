'use strict';

module.exports = (state = {}, action) => {
    switch (action.type) {
        case 'USER_DATA':
            return Object.assign({}, state, {
                phone: action.payload.phone,
                record: action.payload.record,
            });
        default:
            return state;
    };
};
