'use strict';

const { combineReducers } = require('redux');

const user = require('./user');

module.exports = combineReducers({
    user,
});
